<?php
/**
 * Hestia functions and definitions
 *
 * @package Hestia
 * @since   Hestia 1.0
 */

define( 'HESTIA_VERSION', '3.0.1' );
define( 'HESTIA_VENDOR_VERSION', '1.0.2' );
define( 'HESTIA_PHP_INCLUDE', trailingslashit( get_template_directory() ) . 'inc/' );
define( 'HESTIA_CORE_DIR', HESTIA_PHP_INCLUDE . 'core/' );

if ( ! defined( 'HESTIA_DEBUG' ) ) {
	define( 'HESTIA_DEBUG', false );
}

// Load hooks
require_once( HESTIA_PHP_INCLUDE . 'hooks/hooks.php' );

// Load Helper Globally Scoped Functions
require_once( HESTIA_PHP_INCLUDE . 'helpers/sanitize-functions.php' );
require_once( HESTIA_PHP_INCLUDE . 'helpers/layout-functions.php' );

if ( class_exists( 'WooCommerce', false ) ) {
	require_once( HESTIA_PHP_INCLUDE . 'compatibility/woocommerce/functions.php' );
}

if ( function_exists( 'max_mega_menu_is_enabled' ) ) {
	require_once( HESTIA_PHP_INCLUDE . 'compatibility/max-mega-menu/functions.php' );
}

/**
 * Adds notice for PHP < 5.3.29 hosts.
 */
function hestia_no_support_5_3() {
	$message = __( 'Hey, we\'ve noticed that you\'re running an outdated version of PHP which is no longer supported. Make sure your site is fast and secure, by upgrading PHP to the latest version.', 'hestia' );

	printf( '<div class="error"><p>%1$s</p></div>', esc_html( $message ) );
}


if ( version_compare( PHP_VERSION, '5.3.29' ) < 0 ) {
	/**
	 * Add notice for PHP upgrade.
	 */
	add_filter( 'template_include', '__return_null', 99 );
	switch_theme( WP_DEFAULT_THEME );
	unset( $_GET['activated'] );
	add_action( 'admin_notices', 'hestia_no_support_5_3' );

	return;
}

/**
 * Begins execution of the theme core.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function hestia_run() {

	require_once HESTIA_CORE_DIR . 'class-hestia-autoloader.php';
	$autoloader = new Hestia_Autoloader();

	spl_autoload_register( array( $autoloader, 'loader' ) );

	new Hestia_Core();

	$vendor_file = trailingslashit( get_template_directory() ) . 'vendor/composer/autoload_files.php';
	if ( is_readable( $vendor_file ) ) {
		$files = require_once $vendor_file;
		foreach ( $files as $file ) {
			if ( is_readable( $file ) ) {
				include_once $file;
			}
		}
	}
	add_filter( 'themeisle_sdk_products', 'hestia_load_sdk' );

	if ( class_exists( 'Ti_White_Label', false ) ) {
		Ti_White_Label::instance( get_template_directory() . '/style.css' );
	}
}

/**
 * Loads products array.
 *
 * @param array $products All products.
 *
 * @return array Products array.
 */
function hestia_load_sdk( $products ) {
	$products[] = get_template_directory() . '/style.css';

	return $products;
}

require_once( HESTIA_CORE_DIR . 'class-hestia-autoloader.php' );

/**
 * The start of the app.
 *
 * @since   1.0.0
 */
hestia_run();

/**
 * Append theme name to the upgrade link
 * If the active theme is child theme of Hestia
 *
 * @param string $link - Current link.
 *
 * @return string $link - New upgrade link.
 * @package hestia
 * @since   1.1.75
 */
function hestia_upgrade_link( $link ) {

	$theme_name = wp_get_theme()->get_stylesheet();

	$hestia_child_themes = array(
		'orfeo',
		'fagri',
		'tiny-hestia',
		'christmas-hestia',
		'jinsy-magazine',
	);

	if ( $theme_name === 'hestia' ) {
		return $link;
	}

	if ( ! in_array( $theme_name, $hestia_child_themes, true ) ) {
		return $link;
	}

	$link = add_query_arg(
		array(
			'theme' => $theme_name,
		),
		$link
	);

	return $link;
}

add_filter( 'hestia_upgrade_link_from_child_theme_filter', 'hestia_upgrade_link' );

/**
 * Check if $no_seconds have passed since theme was activated.
 * Used to perform certain actions, like displaying upsells or add a new recommended action in About Hestia page.
 *
 * @param integer $no_seconds number of seconds.
 *
 * @return bool
 * @since  1.1.45
 * @access public
 */
function hestia_check_passed_time( $no_seconds ) {
	$activation_time = get_option( 'hestia_time_activated' );
	if ( ! empty( $activation_time ) ) {
		$current_time    = time();
		$time_difference = (int) $no_seconds;
		if ( $current_time >= $activation_time + $time_difference ) {
			return true;
		} else {
			return false;
		}
	}

	return true;
}

/**
 * Legacy code function.
 */
function hestia_setup_theme() {
	return;
}

/*Remove WordPress menu from admin bar*/
function remove_admin_bar_links() {
   global $wp_admin_bar;
//    $wp_admin_bar->remove_menu('wp-logo'); // Hapus WordPress logo
   $wp_admin_bar->remove_menu('about'); // Hapus about WordPress   link
   $wp_admin_bar->remove_menu('wporg'); // Hapus WordPress.org link
   $wp_admin_bar->remove_menu('documentation'); // Hapus WordPress documentation link
   $wp_admin_bar->remove_menu('support-forums'); // Hapus support forums link
   $wp_admin_bar->remove_menu('feedback'); // Hapus feedback link
   $wp_admin_bar->remove_menu('site-name'); // Hapus site name menu
   $wp_admin_bar->remove_menu('view-site'); // Hapus view site link
   $wp_admin_bar->remove_menu('updates'); // Hapus updates link
   $wp_admin_bar->remove_menu('comments'); // Hapus comments link
   $wp_admin_bar->remove_menu('new-content'); // Hapus content link
   $wp_admin_bar->remove_menu('w3tc'); // Jika anda menggunakan w3 total cache Hapus performance link
   $wp_admin_bar->remove_menu('my-account'); // Hapus user details tab
}
add_action( 'wp_before_admin_bar_render', 'remove_admin_bar_links' );

add_action('admin_menu', 'shomtek_admin_menu');

// hide update notifications
function remove_core_updates(){
global $wp_version;return(object) array('last_checked'=> time(),'version_checked'=> $wp_version,);
}
add_filter('pre_site_transient_update_core','remove_core_updates'); //hide updates for WordPress itself
add_filter('pre_site_transient_update_plugins','remove_core_updates'); //hide updates for all plugins
add_filter('pre_site_transient_update_themes','remove_core_updates'); //hide updates for all themes


function wpb_custom_logo() {
echo '
<style type="text/css">
#wpadminbar #wp-admin-bar-wp-logo > .ab-item .ab-icon:before {
background-image: url(' . get_bloginfo('stylesheet_directory') . '/assets/img/logo-custom.png) !important;
background-position: 0 0;
color:rgba(0, 0, 0, 0);
}
#wpadminbar #wp-admin-bar-wp-logo.hover > .ab-item .ab-icon {
background-position: 0 0;
}
</style>
';
}
 
//hook into the administrative header output
add_action('wp_before_admin_bar_render', 'wpb_custom_logo');


// Logo
add_action('admin_menu', 'shomtek_admin_menu');

function shomtek_admin_menu() {
    global $menu;
    $url = 'http://www.shomtek.com/';
    $menu[0] = array( __('SHOMTek'), 'read', $url, 'shomtek-logo', 'shomtek-logo');
}

add_action('admin_head', 'shomtek_admin_style');

function shomtek_admin_style() {
    echo '<link rel="stylesheet" href="' . get_template_directory_uri() . '/assets/css/admin_custom.css" type="text/css" media="all" />';
}
 

// ADD NEW COLUMN
function ST4_columns_head($defaults) {
    $defaults['error_message'] = 'Status';
//     $defaults['test'] = 'test';
    return $defaults;
}
 
// SHOW THE FEATURED IMAGE
function ST4_columns_content($column_name, $post_ID) {
    if ($column_name == 'error_message') {
		
        	$post_error_message = get_post_meta($post_ID);
// 			$post = get_post();
			$error = $post_error_message["_members_access_error"][0];
			if($error and get_post()->post_status !== 'publish') {
				echo "<a href='post.php?post=" . $post_ID . "&action=edit' style='padding: 6px 12px; background: 			    red; color: white; border-radius: 4px; margin: 5px;'>Revision</a>";
			}
    }
	
}

add_filter( 'nav_menu_meta_box_object', 'show_private_pages_menu_selection' );
/**
* Add query argument for selecting pages to add to a menu
*/
function show_private_pages_menu_selection( $args ){
    if( $args->name == 'page' ) {
        $args->_default_query['post_status'] = array('publish','private');
    }
    return $args;
}


add_filter('manage_posts_columns', 'ST4_columns_head');
add_action('manage_posts_custom_column', 'ST4_columns_content', 10, 2);
