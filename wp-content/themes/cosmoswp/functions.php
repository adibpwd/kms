<?php
/**
 * CosmosWP functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package CosmosWP
 * @subpackage CosmosWP
 */


/*Define Constants for this theme*/
define( 'COSMOSWP_VERSION', '1.1.2' );
define( 'COSMOSWP_THEME_NAME', 'cosmoswp' );
define( 'COSMOSWP_PATH', get_template_directory() );
define( 'COSMOSWP_URL', get_template_directory_uri() );
define( 'COSMOSWP_SCRIPT_PREFIX', ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min' );
/**
 * Require init.
 */
require trailingslashit( COSMOSWP_PATH ).'inc/init.php';

function remove_admin_bar_links() {
global $wp_admin_bar;
$wp_admin_bar->remove_menu('wp-logo'); // Hapus WordPress logo
$wp_admin_bar->remove_menu('updates'); // Hapus updates link
$wp_admin_bar->remove_menu('comments'); // Hapus comments link
$wp_admin_bar->remove_menu('new-content'); // Hapus content link
}
add_action( 'wp_before_admin_bar_render', 'remove_admin_bar_links' );

// hide update notifications
function remove_core_updates(){
global $wp_version;return(object) array('last_checked'=> time(),'version_checked'=> $wp_version,);
}
add_filter('pre_site_transient_update_core','remove_core_updates'); //hide updates for WordPress itself
add_filter('pre_site_transient_update_plugins','remove_core_updates'); //hide updates for all plugins
add_filter('pre_site_transient_update_themes','remove_core_updates'); //hide updates for all themes


@ini_set( 'upload_max_size' , '120M' );
@ini_set( 'post_max_size', '120M');
@ini_set( 'max_execution_time', '300' );